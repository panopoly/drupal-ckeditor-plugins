Drupal CKEditor Plugins
=======================

This project generates a Composer repository that contains CKEditor plugins
for use with Drupal 8+ sites.

You can browse the repository here:

https://panopoly.gitlab.io/drupal-ckeditor-plugins/

To use these packages, add this to the "repositories" section of your
site's top-level composer.json, which you can do by hand or by running this
command:

    composer config repositories.drupal-ckeditor-plugins composer https://panopoly.gitlab.io/drupal-ckeditor-plugins

Then you can require CKEditor plugins like:

    composer require drupal-ckeditor-plugin/colorbutton:4.*

And it'll place the code in:

    web/libraries/colorbutton

Contributing
------------

We include a list of all the "core" plugins (meaning those included in the main
CKEditor source code) in the `core-plugins.txt` file, and a list of all the
versions of CKEditor used by an official, support Drupal release made since
this project began in `core-versions.txt`.

Any contrib (ie not "core") plugins come from the `contrib-plugins.json` file.

And the `extra-repositories.json` file includes extra Composer repositories.

If you want a plugin added to this repository, please make an MR which updates
one of those files.

The only hard rule at the moment is that all plugins should come from
https://download.ckeditor.com and be compatible with Drupal.

