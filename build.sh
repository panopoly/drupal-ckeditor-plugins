#!/bin/sh

exec docker run --rm --init \
  -u $(id -u):$(id -g) \
  -v $(pwd):/build \
  -v "${COMPOSER_HOME:-$HOME/.composer}:/composer" \
  composer/satis

