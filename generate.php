#!/usr/bin/env php
<?php

/**
 * @file
 * Generates the satis.json file.
 */

function generate_satis_json($dest, $core_versions, $core_plugins, $contrib_plugins, $extra_repositories) {
  $satis = [
    'name' => 'panopoly/drupal-ckeditor-plugins',
    'description' => 'Drupal CKEditor Plugins',
    'homepage' => 'https://panopoly.gitlab.io/drupal-ckeditor-plugins',
    'repositories' => [],
    'require-all' => TRUE,
  ];

  foreach ($core_versions as $version => $core_requirement) {
    $require = [];
    if (strpos($core_requirement, 'drupal/ckeditor:') === 0) {
      $require['drupal/ckeditor'] = substr($core_requirement, 16);
    }
    else {
      $require['drupal/core'] = $core_requirement;
    }
    foreach ($core_plugins as $plugin) {
      $satis['repositories'][] = [
        "type" => "package",
        "package" => [
          "name" => "drupal-ckeditor-plugin/{$plugin}",
          "version" => $version,
          "type" => "drupal-library",
          "dist" => [
            "url" => "https://download.ckeditor.com/{$plugin}/releases/{$plugin}_{$version}.zip",
            "type" => "zip"
          ],
          "require" => $require,
        ],
      ];
    }
  }

  foreach ($contrib_plugins as $plugin => $versions) {
    foreach ($versions as $version => $url) {
      $satis['repositories'][] = [
        "type" => "package",
        "package" => [
          "name" => "drupal-ckeditor-plugin/{$plugin}",
          "version" => $version,
          "type" => "drupal-library",
          "dist" => [
            "url" => $url,
            "type" => "zip"
          ]
        ]
      ];
    }
  }

  foreach ($extra_repositories as $repository) {
    $satis['repositories'][] = $repository;
  }

  file_put_contents($dest, json_encode($satis, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
}

function read_text_file($file) {
  $lines = explode("\n", file_get_contents($file));
  return array_filter($lines, function ($line) {
    return !empty($line) && $line[0] !== '#';
  });
}

function read_json_file($file) {
  return json_decode(file_get_contents($file), TRUE);
}

function main() {
  $root = dirname(__FILE__);

  $core_versions = [];
  foreach (read_text_file("$root/core-versions.txt") as $core_version_line) {
    $core_version_parts = array_map('trim', explode(':', $core_version_line, 2));
    $core_versions[$core_version_parts[0]] = $core_version_parts[1];
  }

  $core_plugins = read_text_file("$root/core-plugins.txt");

  $contrib_plugins = read_json_file("$root/contrib-plugins.json");

  $extra_repositories = read_json_file("$root/extra-repositories.json");

  generate_satis_json("$root/satis.json", $core_versions, $core_plugins, $contrib_plugins, $extra_repositories);
}

if (php_sapi_name() === 'cli') {
  main();
}

